package com.sword.rpc.client;

/**
 * @author chengyan
 * @date 2022/4/25 18:17
 */
public class SwordClientTest {
    public static void main(String[] args) throws InterruptedException {

        SwordClient client = new SwordClient("localhost", 9000);
        client.start();

    }
}
