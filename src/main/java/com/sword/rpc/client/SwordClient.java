package com.sword.rpc.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;

import java.net.InetSocketAddress;

/**
 * @author chengyan
 * @date 2022/4/25 17:52
 */
public class SwordClient {


    public SwordClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    private String host;
    private int port;

    public void start() throws InterruptedException {

        EventLoopGroup group = new NioEventLoopGroup();

        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group).remoteAddress(new InetSocketAddress(host, port))
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel channel) throws Exception {
                            ByteBuf split = Unpooled.copiedBuffer("~@".getBytes());
                            channel.pipeline()
                                    .addLast(new DelimiterBasedFrameDecoder(1024,split))
                                    .addLast(new SwordClientHandler());
                        }
                    });

            /*异步连接到服务器，sync()会阻塞到完成，和服务器的不同点*/
            ChannelFuture f = bootstrap.connect().sync();
            f.channel().closeFuture().sync();/*阻塞当前线程，直到客户端的Channel被关闭*/
        } finally {
            group.shutdownGracefully().sync();
        }
    }

}
