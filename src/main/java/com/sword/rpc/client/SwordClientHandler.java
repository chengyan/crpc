package com.sword.rpc.client;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

/**
 * @author chengyan
 * @date 2022/4/25 18:24
 */
public class SwordClientHandler extends SimpleChannelInboundHandler<ByteBuf> {
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf) throws Exception {

        System.out.println("client Accept-------" + byteBuf.toString(CharsetUtil.UTF_8));
    }

    /*channel活跃后，做业务处理*/
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {

        for (int i = 0; i < 10; i++) {

            ctx.writeAndFlush(Unpooled.copiedBuffer(
                    "Hello,Netty-"+i+"~@", CharsetUtil.UTF_8));
        }

        ctx.alloc().buffer();
    }
}
