package com.sword.rpc.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import lombok.extern.log4j.Log4j2;

import java.net.InetSocketAddress;

/**
 * sword rpc 服务端
 *
 * @author chengyan
 * @date 2022/4/25 16:59
 */
@Log4j2
public class SwordServer {

    /**
     * 端口号
     */
    private int port;

    public SwordServer(int port) {
        this.port = port;
    }

    public void start() throws InterruptedException {
        EventLoopGroup group = new NioEventLoopGroup();
        try {

            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(group)
                    .localAddress(new InetSocketAddress(port))
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel channel) throws Exception {
                            ByteBuf split = Unpooled.copiedBuffer("~@".getBytes());
                            channel.pipeline()
                                    .addLast(new DelimiterBasedFrameDecoder(1024,split))
                                    .addLast(new SwordServerHandler());
                        }
                    });

            /*异步绑定到服务器，sync()会阻塞到完成*/
            ChannelFuture channelFuture = serverBootstrap.bind().sync();

            log.info("服务器启动完成。。。");
            /*阻塞当前线程，直到服务器的ServerChannel被关闭*/
            channelFuture.channel().closeFuture().sync();
        } finally {
            group.shutdownGracefully().sync();
        }


    }
}
