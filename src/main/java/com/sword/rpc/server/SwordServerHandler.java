package com.sword.rpc.server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;
import lombok.extern.log4j.Log4j2;

/**
 * 服务端业务handler
 *
 * @author chengyan
 * @date 2022/4/25 17:41
 */
@Log4j2
public class SwordServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        ByteBuf byteBuf = (ByteBuf) msg;
        log.info("接受到了消息" + byteBuf.toString(CharsetUtil.UTF_8));

        // 写回客户端
        ctx.writeAndFlush(byteBuf);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {

        log.info("有新的连接建立：" + ctx.name());
        super.channelActive(ctx);
    }
}
