package com.sword.rpc.message;

/**
 *  消息定义
 *  包含两部分：消息头、消息体
 * @author chengyan
 * @date 2022/4/26 13:34
 */

public class Message {

    /**
     * 消息头
     */
    private Header header;

    /**
     * 消息体
     */
    private Object body;


    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "message[header=" + header + "],[body=" + body + "]";
    }
}
