package com.sword.rpc.message;

import java.util.Map;

/**
 * 消息头
 *
 * @author chengyan
 * @date 2022/4/26 13:35
 */
public class Header {

    /**
     * md5
     */
    private String md5;

    /**
     * 消息类型
     */
    private byte type;


    /**
     * 消息ID
     */
    private long messageId;


    /**
     * 优先级
     */
    private long priority;


    /**
     * 可选字段，用于消息头扩展
     */
    private Map<String, String> attachMent;

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    public long getPriority() {
        return priority;
    }

    public void setPriority(long priority) {
        this.priority = priority;
    }

    public Map<String, String> getAttachMent() {
        return attachMent;
    }

    public void setAttachMent(Map<String, String> attachMent) {
        this.attachMent = attachMent;
    }
}
